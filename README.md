# DataScienceandTechnologiesFinalProject
The project is about a business scenario for a travel booking website that wants to improve the customer experience for flights that were delayed. The company wants to create a service to let the customers know the likelihood of the flight being delayed based on the weather conditions before they book the flight to or from the busiest airports in US.

The onpremises.ipynb document will evaluate the problem and create a solution using machine learning to identify how likely the flight will be delayed based on the available weather data. The data is available in .zip format in the repository. The document will build a Logistic Regression model to solve the business problem.

## Getting started

Before continue, the code is written in python in a markdown format. Please ensure that your computer has python installed.

Please create a virtual environment where the dependencies of the project will be installed. Create the virtual environment using the command below.

> python -m venv venv

After create the virtual environment, please activate the virtual environment with the command below.

> venv/Scripts/activate

To deactivate, simply type in deactivate to the command line.

> deactivate

Ensure that the virtual environment is activated. After that, please run the following line to install all dependencies to the virtual environment.

> pip install -r requirements.txt

The command should run pip and install all dependencies to the computer.

## Dataset requirements

Please ensure that you have the flights dataset, named "data_compressed.zip". There is no need to manually extract the zipfile, as the script will do the unzipping.

## Running the code

You can load onpremises.ipynb on any IDE that supports Interactive Python Notebook extension, however we would recommend to use Jupyter Notebook.

Please ensure that you are using the 'venv' kernel to use the dependencies. For jupyter notebook, it is under 'Kernel' tab on the top-left side.

## AWS file

If you have an AWS account, you could run oncloud.ipynb file. Please ensure that you are running with SageMaker service.
WARNING: You might need to pay for accessing the resources needed to train the code.
The data was trained on ml.t3.large notebook instance. Anything lower will crash the code.
